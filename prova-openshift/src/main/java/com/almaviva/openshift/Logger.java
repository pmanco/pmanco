package com.almaviva.openshift;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class Logger
{

	private org.slf4j.Logger logger = LoggerFactory.getLogger("com.almaviva");

	public void debug(String x)
	{
		logger.debug(x);
	}

	public void info(String x)
	{
		logger.info(x);
	}

	public void error(String x)
	{
		logger.error(x);
	}
}

package com.almaviva.openshift.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.almaviva.openshift.Logger;

@RestController
@RequestMapping("/hello")
public class HelloController
{

	@Autowired Logger logger;

	@RequestMapping("/upper")
	public String upper(@RequestParam String x)
	{
		logger.debug("upper di " + x);
		return x.toUpperCase();
	}
}

var p = {};
function renderDIV(e)
{
	e.innerHTML = p[e.getAttribute("x-bind")];
}

function setupINPUT(e)
{
	var o = document.getElementById(e);
	var funzione = function(event)
	{
		p[this.getAttribute("x-bind")] = event.target.value;
		render();
	}
	o.onchange = funzione;
	o.onkeyup = funzione;
}

function renderINPUT(e)
{
	e.value = p[e.getAttribute("x-bind")];
}

function start()
{
	for(var e in document.all){
		if(window["render" + document.all[e].tagName] && !document.all[e].id) document.all[e].id = Math.random(); 
		if(window["setup" + document.all[e].tagName]) window["setup" + document.all[e].tagName](document.all[e].id);
	}
	render();
}

function render()
{
	for(var e in document.all)
	{
		var o = document.getElementById(e);
		if(!o) continue;
		if(window["render" + o.tagName]) window["render" + o.tagName](o);
	}
}

window.onload = start;
